package io.jienba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@SpringBootApplication
@RestController
@RequestMapping("api/v1/customers")
public class Main {
    private final CustomerRepository customerRepository;
    record NewCustomerRequest(
            String name,
            String email,
            Integer age
    ){ }

    public Main(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public static void  main(String [] args){
        SpringApplication.run(Main.class, args);
    }
    @GetMapping()
    public List<Customer> getAllCustomers(){
        return customerRepository.findAll();
    }

    @PostMapping
    public void addCustomer(@RequestBody NewCustomerRequest request){
        System.out.println("Adding customer");
        var customer = new Customer();
        customer.setName(request.name);
        customer.setEmail(request.email);
        customer.setAge(request.age);
        customerRepository.save(customer);
    }

    @DeleteMapping("{customerId}")
    public void deleteCustomer(@PathVariable("customerId") Integer id){
        this.customerRepository.deleteById(id);
    }

    @PutMapping
    public void updateCustomer(@RequestBody Customer customer){
        var customerUpdated = new Customer();
        customerUpdated = this.customerRepository.getReferenceById(customer.getId());
        customerUpdated.setName(customer.getName());
        customerUpdated.setAge(customer.getAge());
        customerUpdated.setEmail(customer.getEmail());
        this.customerRepository.saveAndFlush(customerUpdated);
    }


}
